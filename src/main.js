// @flow

type Input = Array<Array<number>>
type Output = Array<number>

const sum = array => array.reduce((localSum, value) => localSum + value, 0)

export const isArrayOfArray = (value: mixed): boolean => {
  if (Array.isArray(value)) {
    return value.every(item => Array.isArray(item))
  }
  return false
}

export const isCorrectBowlingScore = (score: Input): boolean => {
  const hasGoodNumberOfThrow = score.every((item, index): boolean => {
    if (index === 9 && (item.length === 2 || item.length === 3)) return true
    else if (item.length === 1 || item.length === 2) return true
    return false
  })

  const sums = score.map(currentThrow => sum(currentThrow))

  const hasCorrectMaximalSum = sums.every((item, index): boolean => {
    if (index < 9 && item <= 10) return true
    if (index === 9) return true
    return false
  })

  if (hasGoodNumberOfThrow && hasCorrectMaximalSum) return true
  return false
}

const inputIsInvalid = (input: Input): boolean => {
  if (!isArrayOfArray(input)) return true
  if (!isCorrectBowlingScore(input)) return true

  return false
}

export default (input: Input): Output => {
  if (inputIsInvalid(input)) {
    throw new Error('Invalid Input')
  }

  let previous = 0
  const finalScore = input.map((currentThrow, index, localInput): number => {
    const result = sum(currentThrow)
    let score = result + previous

    if (currentThrow.length === 1) {
      const nextThrow = localInput[index + 1]
      const nextThrowSum = sum(nextThrow)
      score += nextThrowSum
    }
    previous = score

    return score
  })

  return finalScore
}
