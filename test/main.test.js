import finalScore, { isArrayOfArray, isCorrectBowlingScore } from '../src/main'

const input = [
  [5, 2],
  [8, 1],
  [6, 4],
  [10],
  [0, 5],
  [2, 6],
  [8, 1],
  [5, 3],
  [6, 1],
  [10, 2, 6],
]

const output = [7, 16, 26, 41, 46, 54, 63, 71, 78, 96]

test('final score output is correct', () => {
  expect(finalScore(input)).toEqual(output)
})

test('that if input is incorrect it throws an exception', () => {
  expect(() => {
    finalScore([1, 2])
  }).toThrow('Invalid Input')
})

test('if an array is an array of array', () => {
  expect(isArrayOfArray([1, 2])).toBeFalsy()
  expect(isArrayOfArray('string')).toBeFalsy()
  expect(isArrayOfArray(1)).toBeFalsy()
  expect(isArrayOfArray()).toBeFalsy()
  expect(isArrayOfArray([[1], [2, 3, 4]])).toBeTruthy()
})

test('that the score is correct: good number of throw and maximal sum for each throw', () => {
  expect(isCorrectBowlingScore([[1], [2, 3, 4]])).toBeFalsy()
  expect(isCorrectBowlingScore([[1], [2, 3]])).toBeTruthy()
  expect(isCorrectBowlingScore([[10, 1], [2, 3]])).toBeFalsy()
})
