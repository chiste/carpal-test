'use strict'

const OFF = 0
const WARNING = 1
const ERROR = 2

module.exports = {
  parser: 'babel-eslint',
  extends: [
    'airbnb',
    'plugin:jest/recommended',
    'plugin:flowtype/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  plugins: ['jest', 'flowtype', 'eslint-comments'],
  env: {
    es6: true,
    'jest/globals': true,
  },
  rules: {
    semi: [ERROR, 'never'],
    'arrow-parens': [ERROR, 'as-needed'],
    'comma-dangle': [ERROR, 'only-multiline'],
    'eslint-comments/disable-enable-pair': ERROR,
    'eslint-comments/no-duplicate-disable': ERROR,
    'eslint-comments/no-unlimited-disable': ERROR,
    'eslint-comments/no-unused-disable': ERROR,
    'eslint-comments/no-unused-enable': ERROR,
    'flowtype/require-return-type': [
      ERROR,
      'always',
      {
        excludeArrowFunctions: 'expressionsOnly',
      },
    ],
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true,
    },
  },
}
